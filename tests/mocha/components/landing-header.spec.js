describe("Landing header", () => {

  // create the demo element we need to test
  const landingMenu = $('<nav class="js-main-menu"></nav>')

  before( (done) => {
    // we need to insert the button before the tests so we can test it
    $('body').append(landingMenu)
    // in order to be able to test scroll, we need to change the height of the container
    $('body').css({
      height: 2400
    })
    done()
  })

  after( (done) => {
    // this is only for the view of the tests, so it stays at top once it finished
    $(landingMenu).remove()
    $(document).scrollTop(0)
    // clean the body style
    $('body').removeAttr("style")
    done()
  });

  it('should add class submenu-scroll if scroll position >= 100', () => {
    // scroll down more than 100 in order to add the class
    window.scroll(0, 100)

    // call the method
    getScrolledData()

    // expect to find the solid bg class
    expect(landingMenu).to.have.class('solid-bg')
  })

  it('should remove class submenu-scroll if scroll position < 100', () => {
    // scroll up
    window.scroll(0, 99)

    // call the method
    getScrolledData()

    // expect NOT to find the solid bg class
    expect(landingMenu).to.not.have.class('solid-bg')
  })
});
