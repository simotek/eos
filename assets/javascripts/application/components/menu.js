/* Return TRUE/FALSE base on windows size.
========================================================================== */
const isSmallScreen = () => {
  return window.innerWidth <= 1260
}

/* ==========================================================================
  Collapse funtionality
  ========================================================================== */
const collapseSidebarOnSmallScreen = () => {
  if (isSmallScreen()) {
    // in small screen the menu starts collapsed
    $('.js-main-menu').addClass('collapsed-sidebar')
    $('.js-footer-content').addClass('display-collapsed')
    // return true to initiate the tooltip
    return true
  }
}

const collapseSidebarBasedOnLocalStorage = () => {
  if (window.localStorage.getItem('collapsedSidebar') === 'true') {
    // if saved in locastorage that collapsedSidebar=true, menu starts collapsed
    $('.js-main-menu').addClass('collapsed-sidebar')
    // return true to initiate the tooltip
    return true
  }
}

const toggleSidebarAndSave = () => {
  $('.js-main-menu').toggleClass('collapsed-sidebar')
  $('.js-footer-content').toggleClass('display-collapsed')
  toggleTooltip()
  if (!isSmallScreen()) {
    saveSidebarState()
  }
}

const autoToggleSidebar = () => {
  const shouldCollapse = isSmallScreen() || window.localStorage.getItem('collapsedSidebar') === 'true'
  toggleTooltip()
  $('.js-main-menu').toggleClass('collapsed-sidebar', shouldCollapse)
  $('.js-footer-content').toggleClass('display-collapsed', shouldCollapse)
}

const saveSidebarState = () => {
  window.localStorage.setItem('collapsedSidebar', $('.js-main-menu').hasClass('collapsed-sidebar'))
}

// Initiate or destroy the tooltip according to the state of the collapsible menu
const toggleTooltip = () => {
  // if the menu is not collapsed (meaning, it is open) we dont need to show the tooltips
  if ($('.collapsed-sidebar').length === 0) {
    $('.mm-navigation-container li').tooltip('destroy')
    // If sidebar is collapsed update sidebar toggle btn tooltip title
    $('.js-sidebar-tooltip').attr('data-original-title', 'Collapse menu')
  } else {
    // if the menu is collapsed, we initiate de tooltip
    $('.mm-navigation-container li').tooltip()
    // If sidebar is collapsed update sidebar toggle btn tooltip title
    $('.js-sidebar-tooltip').attr('data-original-title', 'Open menu')
  }
}

// Togle on resize and click.
$(window).resize(autoToggleSidebar)
$(document).on('click', '.js-sidebar-toggle', toggleSidebarAndSave)

// When the page is done loading, check if we should toggle the menu based on width or localstorage
$(document).ready(() => {
  if (window.localStorage.getItem('collapsedSidebar') === 'true') {
    // Always display content-footer if showSidebar === false
    $('.js-footer-content').addClass('display-collapsed')
    // If sidebar is collapsed update sidebar toggle btn tooltip title
    $('.js-sidebar-tooltip').attr('data-original-title', 'Open menu')
  }
  // If sidebar isn't collapsed update sidebar toggle btn tooltip title
  if (window.localStorage.getItem('collapsedSidebar') === 'false') {
    $('.js-sidebar-tooltip').attr('data-original-title', 'Collapse menu')
  }

  // if either the screen is small or it is saved in localstorage to keep the menu open
  // then we Initiate the tooltip
  if (collapseSidebarOnSmallScreen() || collapseSidebarBasedOnLocalStorage()) {
    toggleTooltip()
  }
})
