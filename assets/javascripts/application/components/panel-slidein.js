$(document).ready(function () {
  $('.js-panel-slidein-open').on('click', panelSlideinVisible)
  function panelSlideinVisible () {
    $('.js-panel-slidein').addClass('visible')
    closeWithEsc()
  }
  $('.js-panel-slidein-close').on('click', panelSlideinClose)
  function panelSlideinClose () {
    $('.js-panel-slidein').removeClass('visible')
  }
})

const closeWithEsc = () => {
  $(document).keydown((e) => {
    // if keypressed is ESC (keycode 27), hide panel-slidein
    if (e.keyCode === 27) {
      $('.js-panel-slidein').removeClass('visible')
    }
  })
}
