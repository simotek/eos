/* ==========================================================================
  Icons version and links
  ========================================================================== */

/* Prepare local variables that will be reused in different methods */
let $iconsContainer, $iconDisplayTemplate, $tagTemplate

$(function () {
  $('.js-eos-icons-notification').hide()
  // Prepare icons containers
  $iconsContainer = $('.js-eos-icons-list')
  $iconDisplayTemplate = $('.js-icon-display').clone(true)
  $('.js-icon-display').remove()

  // Initiate the full collection of icons
  getFullIconsCollection()

  // Tag span clone
  $tagTemplate = $('.js-eos-icons-tag').clone(true)

  /* Better search response and API call
  ========================================================================== */
  let typingTimer
  const doneTypingInterval = 800

  /* on keyup, start the countdown */
  $('.js-eos-icon-filter').on('keyup', function () {
    $('.js-eos-icons-notification').hide()
    clearTimeout(typingTimer)
    typingTimer = setTimeout(doneTyping, doneTypingInterval)
  })

  /* on keydown, clear the countdown */
  $('.js-eos-icon-filter').on('keydown', function () {
    clearTimeout(typingTimer)
  })

  /* once typing is done */
  function doneTyping () {
    const query = $('.js-eos-icon-filter').val()
    onSearch(query)
  }
})

const onSearch = (q) => {
  $('.js-icon-display').remove()
  if (!q) {
    // If the search came up emtpy, restore all icons
    getFullIconsCollection()
  } else {
    // If there is a search query, consume the search service
    getFilteredIconsCollection(q)
  }
}

const renderIcons = (collection) => {
  for (let i = 0; i < collection.length; i++) {
    const newIconDisplay = $iconDisplayTemplate.clone(true)
    const iconName = typeof collection[i] === 'string' ? collection[i] : collection[i].name
    // Add icon name
    $(newIconDisplay).find('.js-eos-icons').text(iconName)
    $(newIconDisplay).find('.js-icon-name').text(iconName)

    // This hack is added to all elements although it's only used for animated icons
    $(newIconDisplay).find('.js-eos-icons').addClass(`eos-icon-${iconName}`)

    $($iconsContainer).append(newIconDisplay)
  }
  /* Attach the click event on creation */
  $('.js-eos-icons-set .js-panel-slidein-open').on('click', function () {
    const iconSelected = $(this).children('.js-eos-icons').text()
    toggleEosIconInPanel(iconSelected)
  })
}

const linkRepositoryUrl = (url) => {
  $('.js-eos-icons-repo-link').attr('href', url)
}

const addVersionNumber = (v) => {
  $('.js-eos-icons-version').text(v)
}

const addClassToCode = (baseClass) => {
  $('.js-eos-icons-base-class').text(baseClass)
}

const toggleEosIconInPanel = (iconName) => {
  $('.js-eos-icons-name').text(iconName)

  /* Get information for the Do and Dont and the Tags */
  getMoreInfoFromIconService(iconName, function (data) { // eslint-disable-line no-undef
    const notFoundMsg = 'No information available'
    const animatedClass = data ? data.animatedClass || null : null
    const codeExample = data ? data.codeExample || null : null
    const textDo = data ? data.do || notFoundMsg : notFoundMsg
    const textDont = data ? data.dont : notFoundMsg
    const tags = data ? data.tags || notFoundMsg : notFoundMsg
    addIconSpecificData(animatedClass, codeExample, textDo, textDont, tags)
  })
}

const addIconSpecificData = (animatedClass, codeExample, textDo, textDont, tags) => {
  $('.js-eos-icons-animated-class').text(animatedClass)
  $('.js-eos-icons-animated-class').removeClass('hide-ligature')
  if (animatedClass != null) {
    $('.js-eos-icons-animated-class').addClass('hide-ligature')
    $('.js-eos-icons-example-class').html(jQuery.parseHTML(codeExample))
  }
  $('.js-eos-icons-do').html(jQuery.parseHTML(textDo))
  $('.js-eos-icons-dont').html(jQuery.parseHTML(textDont))
  $('.js-hide-container').removeClass('hide')
  if (textDont.length < 5) {
    $('.js-hide-container').addClass('hide')
  }

  $('.js-eos-icons-tag').remove()
  for (let i = 0; i < tags.length; i++) {
    const _tagTemplate = $tagTemplate.clone(true)
    $(_tagTemplate).text(tags[i])
    $('.js-eos-icons-tags-list').append(_tagTemplate)
  }
}

/* Declared functions
========================================================================== */
/*
 In order to access this function from Unit Testing, we need to make it a declared function
 dont change this to arrow function or unit test will break
*/
// don't use the arrow function here
function getFilteredIconsCollection (q) {
  searchIconsService(q, function (searchResult) { // eslint-disable-line no-undef
    const iconsMerged = searchResult
    renderIcons(iconsMerged)
  })
}
// don't use the arrow function here
function getFullIconsCollection () {
  getIconsCollectionService((dataGlyph, dataPackage) => { // eslint-disable-line no-undef
    // Data manipulation from glyph-list.json
    const dataGlyphCollection = dataGlyph[0]

    const { repositoryUrl } = dataGlyphCollection
    linkRepositoryUrl(repositoryUrl)

    const { baseClass } = dataGlyphCollection
    addClassToCode(baseClass)

    const { glyphs } = dataGlyphCollection
    const iconsAnimated = dataGlyphCollection.animatedIcons
    // Merge static & animated icon objects
    Object.assign(glyphs, iconsAnimated)
    const iconsMerged = Object.assign([], glyphs, iconsAnimated)

    renderIcons(iconsMerged)

    // Data manipulation from package.json
    const dataPackageCollection = dataPackage[0]
    const { version } = dataPackageCollection
    addVersionNumber(version)
  })
}
