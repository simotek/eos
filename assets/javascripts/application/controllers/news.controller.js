$(function () {
  dashboardNews()
})

const dashboardNews = () => {
  getNews(_data => { // eslint-disable-line no-undef
    /* Manage data from Strapi and reduce it to a video and article array */
    const data = _data.data.news.reduce((acc, cur) => {
      /* Push the videos to array if enabled */
      if (cur.selectType === 'video' && cur.enabled === true) {
        acc.videos.push({
          ...cur
        })
      }

      /* Push the articles to array if enabled */
      if (cur.selectType === 'article' && cur.enabled === true) {
        acc.articles.push({
          ...cur
        })
      }

      return acc
    }, { videos: [], articles: [] })

    const sectionTarget = $('.news-tutorials-carousel')

    /* Copy of the template for videos and news */
    const $video = $('.js-video-container').clone(true)
    const $article = $('.js-article-container').clone(true)

    /* Remove blocks from landing after saving a copy in memory */
    $('.js-video-container').remove()
    $('.js-article-container').remove()

    /* We send the data, templates and target to createNewsBox */
    createNewsBoxes(data, sectionTarget, $video, $article)
    /* Get video only when the box is clicked */
    videoLoadOnClick()
    /* Init carousel after appending all the data */
    initCarousel()
  })
}

/* Create news boxes for videos and articles */
const createNewsBoxes = (data, target, video, article) => {
  /* Append all the articles (ele = Element) */
  data.articles.map(ele => addBoxToPage(target, article, ele))

  /* Append all the videos */
  data.videos.map(ele => addBoxToPage(target, video, ele))
}

/* Function that generate the box for videos or articles */
const addBoxToPage = (target, template, data) => {
  const _template = template.clone(true)
  const articleRoute = symbolsParser(data.title)

  const regEx = /[0-9]*-[0-9]*-[0-9]*/gm
  const useDate = data.date.match(regEx)

  /* Set title, thumbnail and url */
  _template.find('h3').text(data.title)
  _template.find('img').attr('src', data.thumbnail.url).load()
  _template.find('.card-date').text(useDate)
  data.selectType === 'video' ? _template.attr('data-video-url', data.videourl) : _template.attr('href', `news/${articleRoute}`)

  /* Apply box color */
  _template.css({ color: data.cardColor, visibility: 'visible' })
  _template.find('footer').css({ 'background': data.cardColor })

  return target.append(_template)
}

/* Function that only load video source on click */
const videoLoadOnClick = () => {
  $('.js-launch-modal').on('click', function () {
    /* Check which video we should load  */
    const videoToLoad = $(this).attr('data-video-url')

    /* Add the src attribute to the video element */
    $('.js-video-src').attr('src', videoToLoad)
    /* This is needed for "load" the video once the src is set */
    $('.js-video-modal video')[0].load()
  })
}

/* Converte the new title so we can use as route */
const symbolsParser = (str) => {
  const regEx = /(\w+)/g
  return str.toLowerCase().match(regEx).join('-')
}

/* Init carousel for dashboard */
const initCarousel = () => {
  /* Init the carousel */
  $('.js-news-tutorials-carousel').slick({
    infinite: false,
    speed: 300,
    slidesToShow: 2,
    slidesToScroll: 1,
    prevArrow: '.js-news-tutorials-before',
    nextArrow: '.js-news-tutorials-next',
    responsive: [
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1
        }
      }
    ]
  })
}
