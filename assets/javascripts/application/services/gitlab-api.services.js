function gitlabAPICall (callback) { // eslint-disable-line no-unused-vars
  $.when(
    $.ajax({
      url: '/api/gitlab',
      dataType: 'json',
      error: function (xhr, status, error) {
        console.error(`There was an error in the request: ${error}`)
      }
    }))
    .then(function (gitlabResponse) {
      callback(gitlabResponse)
    })
}
