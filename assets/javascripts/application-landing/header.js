/* Change header after having scrolled
   ========================================================================== */
/* Initial variables */
const maxScrolling = '100'
let scrolled, scrolledMax

$(window).bind('scroll', function () {
  getScrolledData()
})

const getScrolledData = () => {
  scrolled = $(window).scrollTop()

  /**
  * Detect if the user has scrolled more than the first section (height)
  * to then apply the changes in the header
  */
  if (scrolled >= maxScrolling) {
    headerChanges()
    scrolledMax = true
  }

  if (scrolledMax && scrolled < maxScrolling) {
    headerReset()
    scrolledMax = false
  }
}

const headerChanges = () => {
  $('.js-main-menu').addClass('solid-bg')
}

const headerReset = () => {
  $('.js-main-menu').removeClass('solid-bg')
}
